summary.gpa.result <- function(obj) {
  stopifnot(class(obj) == "gpa.result")
  
  df = data.frame("Nb images" = length(obj$gpa_list),
                  'shape filter' = ifelse(is.null(obj$shape.ind),"FALSE","TRUE"),
                  'growth filter' = ifelse(is.null(obj$growth.check),"FALSE","TRUE"),
                  'Nb removed images' = sum(obj$outlier.vec)
  )
  
  return(df)
}


##############################################################
## Name: plot.gpa.result
## Aim: Plot results from screening function
##
## INPUT:
##  - obj: object of gpa.result class (screening function output )
##  - frame: the type of plot to be drawn.  Must be a vector of "results", "shape" and/or "growth".
##  - options.fig: list of the plot options.
##
## OUTPUT: 
##  - plot
##
##
##############################################################


plot.gpa.result <- function(obj,
                            frames = c("results", "shape", "growth"),
                            options.fig = NULL) {
  
  ## Check
  stopifnot(class(obj) == "gpa.result")
  
  ## Get options
  options.fig = setPlotOption(options.fig)
  
  if (!any(c("results", "shape", "growth") %in% frames)) stop("Undefined frame")
  if ("results" %in% frames & 
      any(!(options.fig$indicators %in% c("Var", "Mean", "CV", "Median", "IQI", "Kurtosis","Skewness", "Q", "Galtonk")))){
    stop('At least one indicators is unknown. You should selected indicators within c("Var", "Mean", "CV", "Median", "IQI", "Kurtosis","Skewness", "Q", "Galtonk")')
  }
  
  gg_res = list(); gg_shape = NULL; gg_growth = NULL
  
  
  if ("results" %in% frames) {
    
    for(k in 1:length(options.fig$indicators)){
      indicator = which(names(obj$ind_gfp[[1]]) %in% options.fig$indicators[k])
      
      val = c()
      for(i in 1:length(obj$ind_gfp)) val[i] = ifelse(is.null(obj$ind_gfp[[i]][indicator]),NA,obj$ind_gfp[[i]][indicator])
      val = unlist(val)
      gg_res[[k]] = .plot_indicator(val,
                                    x.min.cond = options.fig$x.min.cond,
                                    x.max.cond = options.fig$x.max.cond,
                                    title = paste("Indicator:", options.fig$indicators[k]), outvec = obj$outlier.vec)
    }
    # print(gg_res[[1]])
    suppressWarnings(do.call("grid.arrange", c(gg_res, ncol = min(length(gg_res),3))))
  }
  
  if ("shape" %in% frames) {
    gg_shape = ggplot(obj$shape.ind$df_crit, aes(x = time, y = Shape.index)) + annotate(
      "rect",
      xmin = options.fig$x.min.cond - 0.5,
      xmax = options.fig$x.max.cond + 0.5,
      ymin = -Inf,
      ymax = Inf,
      fill = "grey",
      alpha = .5
    ) + geom_point()
    gg_shape = gg_shape + geom_hline(aes(yintercept = thresh), colour = "red")  + theme_minimal() + ggtitle("Shape index")
    if (!("growth" %in% frames)) suppressWarnings(print(gg_shape))
  }
  
  if ("growth" %in% frames) {
    gg_growth = ggplot(obj$growth.check$df_area, aes(x = time, y = Log_area)) + annotate(
      "rect",
      xmin = options.fig$x.min.cond - 0.5,
      xmax = options.fig$x.max.cond + 0.5,
      ymin = -Inf,
      ymax = Inf,
      fill = "grey",
      alpha = .5
    )
    gg_growth = gg_growth + geom_point(aes(color = status)) +
      scale_colour_manual(values = c("kept" = "black", "outliers" = "red")) + stat_smooth(method = "rlm", col = "grey50") + theme_minimal() +
      ggtitle(paste(
        "Robust linear regression (Growth) - R-squared:",
        round(obj$growth.check$r.squared, 3)
      ))
    
    if (!("shape" %in% frames)) suppressWarnings(print(gg_growth))
  }
  
  if (sum(c("shape","growth") %in% frames)==2) suppressWarnings(cowplot::plot_grid(gg_shape,gg_growth,labels = c("A", "B"),ncol = 2,nrow = 1))
}
