##############################################################
## Name: compute_indicators
## Aim: Compute indicators
##
## INPUT:
##  - x : vector of values
##  - na.rm : logical. Should missing values be removed?
##
## OUTPUT: 
##  - List of indicators
##
##
##############################################################


compute_indicators <- function(x,na.rm=TRUE){
  
  
  varW = var(x[!is.na(x)],na.rm=na.rm)
  meanW = mean(x[!is.na(x)],na.rm=na.rm)
  cvW = sqrt(varW)/meanW
  median = median(x[!is.na(x)],na.rm=na.rm)
  iqi = quantile(x[!is.na(x)], 0.75,na.rm=na.rm) - quantile(x[!is.na(x)], 0.25,na.rm=na.rm)
  kurtosis = moments::kurtosis(x[!is.na(x)],na.rm=na.rm)
  skewness = moments::skewness(x[!is.na(x)],na.rm=na.rm)
  Q = quantile(x[!is.na(x)], c(0.25, 0.5, 0.75),na.rm=na.rm)
  galtonk = (Q[1] + Q[3] - 2*Q[2])/(Q[3] - Q[1]) 
  
  # Quantile and variance for dsred (use as a filter ?)
  q99 = quantile(c(x), 0.99, na.rm = TRUE)
  var_dsred = var(c(x), na.rm = TRUE)
  
  out = list(Var = varW, Mean = meanW, CV = cvW, Median = median, IQI = iqi, Kurtosis = kurtosis,
  Skewness = skewness, Q = Q, Galtonk = galtonk,q99=q99,var_dsred=var_dsred)
  
  return(out)
}