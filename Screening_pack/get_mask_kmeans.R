##############################################################
## Name: get_mask_kmeans
## Aim: Get mask from dsred
##
## INPUT:
##  - dsred: List of matrices (n,n) correponding to the reference (dsred)
##  - centers : number of clusters
##
## OUTPUT: 
##  - List of binary matrices (n,n) correponding to the mask
##
##
##############################################################

get_mask_kmeans <- function(dsred,kmeans.options){
  
  ## Init
  n_images = ifelse(is.list(dsred),length(dsred),1)
  if(!is.list(dsred)) dsred = list(dsred) 
  if(!is.list(mask)) mask = list(mask)
  mask = list()
  mask_all = list()
  
  ## Loop over the number of images
  for(k in 1:n_images)
  {
    ## Apply gaussian filter before kmeans
    dsred_tmp = suppressWarnings(applyFilter(x = dsred[[k]], kernel = convKernel(sigma = 2, k = "gaussian")))
    
    ## Apply kmeans
    km = kmeans(c(dsred_tmp), centers = kmeans.options$centers)
    
    ## Keep all the clusters from kmeans clustering
    mask_all[[k]] = matrix(nrow = nrow(dsred_tmp), km$cluster)
    
    ## Binary mask
    tmp = matrix(NA,nrow=nrow(dsred_tmp),ncol=nrow(dsred_tmp))
    tmp[km$cluster==which.max(km$centers)] = 1
    tmp[km$cluster!=which.max(km$centers)] = NA
    mask[[k]] = tmp
    
  }
  
  return(list(mask=mask,mask_all=mask_all))
}

