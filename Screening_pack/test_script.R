
# setwd("~/Documents/Mistreta/Creation_package/vmc-screening/Screening_pack")
setwd(dir = "Documents/vmc-screening/Screening_pack/")
source("loading.R")
dsred_list = list()
gfp_list=list()
mask_list=list()


mask <- readTIFF("data/MIT_S2R1/MASK_Rename/S2R1_Point_278_MIT_Mask.tif", all = T, as.is = T)
dsred <- readTIFF("data/MIT_S2R1/DSRED_Rename/S2R1_Point_278_MIT_DSRed.tif", all = T, as.is = T)
gfp  <- readTIFF("data/MIT_S2R1/GFP_Rename/S2R1_Point_278_MIT_GFP.tif", all = T, as.is = T)

mask_list[[1]] <- readTIFF("data/MIT_S2R1/MASK_Rename/S2R1_Point_278_MIT_Mask.tif", all = T, as.is = T)
dsred_list[[1]] <- readTIFF("data/MIT_S2R1/DSRED_Rename/S2R1_Point_278_MIT_DSRed.tif", all = T, as.is = T)
gfp_list[[1]]  <- readTIFF("data/MIT_S2R1/GFP_Rename/S2R1_Point_278_MIT_GFP.tif", all = T, as.is = T)

mask_list[[2]] <- readTIFF("data/MIT_S2R1/MASK_Rename/S2R1_Point_279_MIT_Mask.tif", all = T, as.is = T)
dsred_list[[2]] <- readTIFF("data/MIT_S2R1/DSRED_Rename/S2R1_Point_279_MIT_DSRed.tif", all = T, as.is = T)
gfp_list[[2]]  <- readTIFF("data/MIT_S2R1/GFP_Rename/S2R1_Point_279_MIT_GFP.tif", all = T, as.is = T)


res = screening (dsred,gfp,mask = mask, kmeans_mask = T)

do.call(rbind,res$ind_gfp)
res$area

res2 = screening (dsred,gfp,mask = mask, kmeans_mask = F,global.options = list(disk=FALSE))
# res2 = screening (dsred,gfp,mask = mask, kmeans_mask = F,global.options = list(allowParallel=FALSE))
res_meta = meta.screening(dsred_list,gfp_list,mask_list)



### Script Maxime

setwd(dir = "Documents/vmc-screening/Screening_pack/")
source("loading.R")
dsred_list = list()
gfp_list=list()
mask_list=list()

list_files_gfp = list.files("/Volumes/VMCscreening/ALLDATA/experiment_2022_july/SCR9_ALL/GFP_Rename",full.names = T)
list_files_dsred = list.files("/Volumes/VMCscreening/ALLDATA/experiment_2022_july/SCR9_ALL/DSRed_Rename/",full.names = T)
list_files_mask = list.files("/Volumes/VMCscreening/ALLDATA/experiment_2022_july/SCR9_ALL/Mask_Rename/",full.names = T)




n_mol = length(list_files_gfp)
results = list()
numCores <- parallel::detectCores()

if(.Platform$OS.type =="windows") numCores = 1
# message("Screening...",appendLF=FALSE)
for(i in 1:50){
  mask_list<- readTIFF(list_files_mask[i], all = T, as.is = T)
  dsred_list <- readTIFF(list_files_dsred[i], all = T, as.is = T)
  gfp_list  <- readTIFF(list_files_gfp[i], all = T, as.is = T)
  
  message(paste("Point",i,"\n"),appendLF=FALSE)
  tmp  = screening(dsred_list,gfp_list,mask_list,kmeans_mask = T,global.options = list(disk=FALSE))
  if(is.null(tmp$ind_gfp)) {
    df_mean = cbind(df_mean,rep(NA,nrow(df_mean)))
    df_var = cbind(df_var,rep(NA,nrow(df_mean)))
    df_area =  cbind(df_area,rep(NA,nrow(df_mean)))
  }
  if(!is.null(tmp$ind_gfp)) {
    if(i==1){
      df_mean=  data.frame(unlist(do.call(rbind,tmp$ind_gfp)[,2]))
      df_var= data.frame(unlist(do.call(rbind,tmp$ind_gfp)[,1]))
      df_area = data.frame(tmp$area)
    }
    if (i!=1){
      df_mean = cbind(df_mean,unlist(do.call(rbind,tmp$ind_gfp)[,2]))
      df_var = cbind(df_var,unlist(do.call(rbind,tmp$ind_gfp)[,1]))
      df_area = cbind(df_area,tmp$area)
    }
  }
}
write.csv(df_mean,file = '../../output_mean.csv')
write.csv(df_var,file = '../../output_var.csv')
write.csv(df_area,file = '../../output_area.csv')
# results <-mclapply(1:n_mol, FUN = function(i){print(i);screening(dsred_list[[i]],gfp_list[[i]],mask_list[[i]],...)} ,mc.cores=max(numCores-2,1),mc.silent = T)
# mc.cores = ifelse(global.options$allowParallel,max(numCores-2,1),1))


